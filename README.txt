There is a folder for each day of class, starting from Wednesday 2013-02-27.

The database used for that day is provided in the form of a mysqldump.
You can import the mysqldump by running the following command (assuming you have not set a root password):
mysql -u root DATABASENAME < THEPATHTOTHEMYSQLDUMPFILEGOESHERE

Notice that you must pass a database name to mysql. This means you must create the database first. 
The database definition itself is not included in the mysqldump file.