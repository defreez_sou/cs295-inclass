<?php

/**
 * IN CLASS CODE FROM 2/27
 * This demontrates how to connect to a database and run parameterized queries
 */

 /**
  * The Atom class is the structure of data that we want to store.
  * Here we are thnk of atoms as in physics.
  */
class Atom 
{
	/** We define what properties exist for our thing.
	 * These should be columns somewhere in our database
	 * It is not strictly necessary to define the properties here,
	 * but I think it is helpful.
	 */
	public $size;			// Big or small 
	public $electrons;		// # of electrons
	public $protons;		// # of protons
	public $neutrons;		// # of neutrons
	
	// Our constructor to actually assign the properties values.
	function __construct($size, $protons, $neutrons, $electrons)
	{
		$this->size = $size;
		$this->protons = $protons;
		$this->neutrons = $neutrons;
		$this->electrons = $electrons;
	}
}

/**
 * The AtomModel contains all of the SQL necessary to create/read/update/delete
 * atoms in the database.
 */ 
class AtomModel
{
	/**
	 * The save method saves an atom to the database.
	 */
	function save($atom) {
		// Connect to the database
		$conn = new mysqli("localhost", "root", "", "myapp");
		// Setup our query
		$query = $conn->prepare("INSERT INTO atom (size, protons, electrons, neutrons) VALUES (?, ?, ?, ?)");
		// Substitute the user values to complete the query
		$query->bind_param("siii", $atom->size, $atom->protons, $atom->electrons, $atom->neutrons);		
		// Actually execute the query
		$query->execute();
	}
	
	/*
	 * This is where the code would go to delete an atom from the database
	 */
	function delete($atom) {
	
	}
	
	/*
	 * This is where the code would go to retrieve all of the atoms from the database
	 */
	function find_all() {}
	
	/*
	 * This retrieves all of the atoms with a particular size.
	 * This is an example of retrieving data using a parameterized query.
	 */
	function find_by_size($size) {
		$atoms = array();
	
		// SELECT FROM atom WHERE size=small
		$conn = new mysqli("localhost", "root", "", "myapp");
		$query = $conn->prepare("SELECT * FROM atom WHERE size=?");
		$query->bind_param("s", $size);		
		$query->execute();
		$query->bind_result($res_size, $res_protons, $res_neutrons, $res_electrons);
		while ($query->fetch()) {
			$atoms[] = new Atom($res_size, $res_protons, $res_neutrons, $res_electrons);
		}
		
		return $atoms;
	}
	
}

/*
 * This is example code that demonstrates the use of our model.
 * This code would go in the controller.
 */

// Create an AtomModel object, so that we can talk to the database
$model = new AtomModel();

// Create an atom object - this is a particular atom
$a = new Atom("small", 5, 6, 7);
// Save our atom to the database
$model->save($a);

// Retrieve all of the small atoms from the database
foreach ($model->find_by_size("small") as $a) {
	print_r($a);
	echo "<br>\n";
}